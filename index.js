// import module
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const fs = require("fs");

// use middleware
app.use(bodyParser.urlencoded({ extends: false }));

// use view engine
app.use(express.static("public"));
app.set("view engine", "ejs");

// middleware login to page landingpage
const authenticateUser = (req, res, next) => {
  const { email, password } = req.body;

  // read file users.json
  const userData = JSON.parse(fs.readFileSync("users.json"));

  // find user email and password user in users.json
  const user = userData.users.find(
    (user) => user.email === email && user.password === password
  );
  // if statement, if user can pass middleware
  if (user) {
    next();
  } else {
    res.render("login-wrongID", { textErr: "Incorrect Email or Password" });
  }
};

// middleware input
app.post("/login", authenticateUser, (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  res.redirect("/");
});

// routing to the /
app.get("/", (req, res) => {
  res.render("landing-page");
});

// routing to the /work
app.get("/work", (req, res) => {
  res.render("work");
});

// routing to the /contact
app.get("/contact", (req, res) => {
  res.render("contact");
});

//routing to the /aboutme
app.get("/aboutme", (req, res) => {
  res.render("aboutme");
});

//routing to the /top-scores
app.get("/top-scores", (req, res) => {
  res.render("top-scores");
});

//routing to the /news
app.get("/news", (req, res) => {
  res.render("news");
});

//routing to the /login
app.get("/login", (req, res) => {
  res.render("login");
});
app.get("/sign-up", (req, res) => {
  res.render("sign-up");
});

//routing to the /game-suit
app.get("/game-suit", (req, res) => {
  res.render("game-suit");
});

// turnon the server on http://localhodt:3000
app.listen(3000, () => {
  console.log("server on http://localhost:3000");
});
